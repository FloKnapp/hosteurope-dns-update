<?php

namespace Lib\Http\Cache;

use Lib\Http\Client;

class Cache
{

    const CACHE_VALID = 86400; // in seconds

    /** @var boolean */
    protected  $active = false;

    /** @var Client */
    protected $client;

    /** @var boolean */
    protected $isDebug = false;

    /**
     * Cache constructor.
     * @param Client $client
     * @param boolean $debug
     */
    public function __construct(Client $client, $debug = false)
    {
        $this->client  = $client;
        $this->isDebug = $debug;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    public static function validateDir()
    {
        if (!is_dir(CACHE_DIR)) {
            mkdir(CACHE_DIR, 0700);
        }

        if (file_put_contents(CACHE_DIR . '/test.cache', 'test')) {

            if (file_get_contents(CACHE_DIR . '/test.cache') === 'test') {
                unlink(CACHE_DIR . '/test.cache');
                return true;
            }

        }

        throw new \Exception('Cache directory cannot be created, please check permissions', 2);
    }

    public static function invalidate()
    {
        $files = scandir(CACHE_DIR);

        foreach ($files as $file) {

            if ($file === '.' || $file === '..') {
                continue;
            }

            if (!unlink(CACHE_DIR . '/' . $file)) {
                throw new \Exception('Cannot delete cache file ' . $file);
            }

        }

        return true;
    }

    /**
     * @param string $url
     * @return string
     */
    public function get($url = '')
    {
        $cacheFileName = md5($url) . '.cache';

        if (file_exists(CACHE_DIR . '/' . $cacheFileName)
            && filemtime(CACHE_DIR . '/' . $cacheFileName) > time() - self::CACHE_VALID
        ) {
            if ($this->isDebug) {
                print "CACHE HIT: " . explode('&kdnummer', $url)[0] . "\n";
            }

            return file_get_contents(CACHE_DIR . '/' . $cacheFileName);
        }

        $this->setActive(false);

        $response = $this->client->send($url);

        file_put_contents(CACHE_DIR . '/' . $cacheFileName, $response->getContent());

        $this->setActive(true);

        return $response->getContent();
    }

}